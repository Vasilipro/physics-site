### Электролиз:

Масса $$ m $$ вещества, выделившегося на электроде, прямо пропорциональна заряду $$ Q $$:

$$
m = kQ = kIt
$$

Где:
- $$ k $$ - электрохимический эквивалент.
- $$ I $$ - ток.
- $$ t $$ - время.

Электрохимический эквивалент $$ k $$ может быть рассчитан по формуле:

$$
k = \frac{M}{e \cdot n \cdot N_A}
$$

Где:
- $$ n $$ - валентность вещества.
- $$ N_A $$ - постоянная Авогадро.
- $$ M $$ - молярная масса вещества.
- $$ e $$ - элементарный заряд.

Иногда также используют обозначение: 

$$
F = eN_A
$$

где $$ F $$ - постоянная Фарадея.
